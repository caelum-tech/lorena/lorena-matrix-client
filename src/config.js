require('dotenv').config()

class Config {
  constructor () {
    this.matrixServerURL = process.env.CEVD_MATRIX_SERVER_URL
    this.matrixDaemonUserId = process.env.CEVD_MATRIX_DAEMON_USER_ID
    this.matrixLobby = process.env.CEVD_MATRIX_LOBBY
    this.matrixClientUserId = process.env.CVC_MATRIX_CLIENT_USER_ID
    this.matrixClientPassword = process.env.CVC_MATRIX_CLIENT_PASSWORD
    this.matrixClientAccessToken = process.env.CVC_MATRIX_CLIENT_ACCESS_TOKEN
  }
}

module.exports = new Config()
