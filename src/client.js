const fs = require('fs')
const helpers = require('@caelum-tech/lorena-matrix-helpers')
const JSZip = require('jszip')
const sdk = require('matrix-js-sdk')
const vcr = require('@caelum-tech/lorena-vcr')

const roomCallbacks = {}

/**
 * General callback for messages which calls room-specific callbacks
 *
 * @param {event} event - Matrix room event
 * @param {string} room - Matrix room
 * @param {*} toStartOfTimeline - unused
 */
function handleCommand (event, room, toStartOfTimeline) {
  if (event.getType() !== 'm.room.message') {
    return // only use messages
  }

  if (roomCallbacks[room.roomId]) {
    const { callback, data } = roomCallbacks[room.roomId]
    // filter out our own messages
    const client = data
    if (event.sender.userId !== client.client.getUserId()) {
      callback(event.event, data)
    }
  }
}

/**
 * Called when the user is invited to a new room. (Matrix guest user flow)
 *
 * @param {*} roomId Matrix room ID
 * @param {*} data user data (the client object)
 */
function invitedRoomCallback (roomId, data) {
  const client = data

  // set up a callback specific to this room, if defined
  if (client._caelumResponseRoomCallback) {
    client.createRoomCallback(roomId, client._caelumResponseRoomCallback, data)
  }

  // Send the credential, if defined
  if (client._caelumCredentialToSend) {
    client.sendCredentialToRoom(roomId, client._caelumCredentialToSend)
  }
}

/**
 * Manages connection to a verification bot via Matrix
 */
class Client {
  /**
   * Initialize client and begin sync
   *
   * @param {string} baseUrl - Matrix server URL
   * @param {string} accessToken - Matrix access token
   * @param {string} userId - Matrix user ID
   * @param {Function} invitedRoomCallback - (optional) callback function called upon being invited to a room
   * @returns {string} the state of the client
   */
  async init (baseUrl, accessToken, userId, invitedRoomCallback = undefined) {
    this.client = sdk.createClient({ baseUrl, accessToken, userId })

    // Guest users cannot create rooms, so they have to join a public room where they
    // can ask to be invited to a private room someone else creates.
    if (invitedRoomCallback) {
      this.client.setGuest(true)
      this.client._caelumClient = this

      // automatically join rooms when invited
      this.client.on('RoomMember.membership', function (event, member) {
        if (member.membership === 'invite' && member.userId === userId) {
          this.joinRoom(member.roomId).done(() => {
            console.log(`User ${userId} auto-joined ${member.roomId}`)
            invitedRoomCallback(member.roomId, this._caelumClient)
          })
        }
      })
    }

    await this.client.startClient({ initialSyncLimit: 10, userId })
    this.client.on('Room.timeline', handleCommand)

    // TODO E2E Crypto: https://github.com/matrix-org/matrix-js-sdk#end-to-end-encryption-support

    return this.sync()
  }

  /**
   * Synchronize the client state with the Matrix server
   *
   * @returns {string} state of client
   */
  async sync () {
    return new Promise((resolve, reject) => {
      this.client.once('sync', function (state, prevState, res) {
        resolve(state)
      })
    })
  }

  /**
   * Create a new room and invite the specified user
   *
   * @param {string} userIdToInvite - Matrix user ID to invite to the new room
   * @returns {string} roomId
   */
  async createRoomAndInvite (userIdToInvite) {
    return new Promise((resolve, reject) => {
      this.client.createRoom({
        visibility: 'private', // Either 'public' or 'private'.
        invite: [userIdToInvite] // A list of user IDs to invite to this room.
      }, (err, result) => {
        if (err) {
          console.log(err)
        }
        resolve(result.room_id)
      })
    })
  }

  /**
   * Send a message as text to a room
   *
   * @param {string} roomId - Matrix room ID for posting
   * @param {string} body - message as text
   * @returns {*} event ID
   */
  async sendTextMessage (roomId, body) {
    return this.sendMessage(roomId, { body, msgtype: 'm.text' })
  }

  /**
   * Send a file to a room
   *
   * @param {string} roomId - Matrix room ID for posting
   * @param {Buffer} file - file to send
   * @returns {string} event ID
   */
  async sendFileMessage (roomId, file) {
    const filename = 'verifiableCredential.zip'
    const filetype = 'application/zip'
    let result

    // TODO: Encrypt file with public key for DID of recipient

    try {
      result = await this.client.uploadContent(file, { name: filename, includeFilename: false, rawResponse: false, type: filetype })
      console.log(result)
    } catch (e) {
      console.log(e)
    }
    return this.sendMessage(roomId, { filename, body: filename, msgtype: 'm.file', info: { mimetype: filetype, size: file.length }, url: result.content_uri })
  }

  /**
   * Send a message to a room
   *
   * @param {string} roomId - Matrix room ID for posting
   * @param {*} content - content and message type
   * @returns {string} event ID
   */
  async sendMessage (roomId, content) {
    return new Promise((resolve, reject) => {
      this.client.sendMessage(roomId, content, (err, res) => {
        if (err) {
          console.log(err)
          reject(err)
        } else {
          resolve(res.event_id)
        }
      })
    })
  }

  /**
   * Send the credential (in JSON format) to the specified room.
   *
   * @param {string} roomId - Matrix room ID for posting
   * @param {*} credential - credential in Buffer or JSON
   * @returns {string} event ID
   */
  async sendCredentialToRoom (roomId, credential) {
    if (Buffer.isBuffer(credential)) {
      return this.sendFileMessage(roomId, credential)
    } else {
      return this.sendTextMessage(roomId, JSON.stringify(credential))
    }
  }

  /**
   * Create a callback function that is called for any notice messages posted
   * to the specified room.  Allows for user data to accompany the callback.
   *
   * @param {string} roomId - Matrix room ID for posting
   * @param {Function} callback - callback function
   * @param {*} data - data to pass to the callback (usually this Client object)
   */
  createRoomCallback (roomId, callback, data = undefined) {
    roomCallbacks[roomId] = { callback, data }
  }

  /**
   * Zip multiple files together into a single file
   *
   * @param {JSON} credential text to be sent as the VC
   * @param {*} attachments files to be added
   * @returns {*} credential object or nodebuffer containing zip of credential and attachments
   */
  async makeSingleCredentialFile (credential, attachments = []) {
    if (attachments.length > 0) {
      const zip = new JSZip()
      zip.file('credential.json', JSON.stringify(credential))
      attachments.forEach(attachment => {
        zip.file(attachment.name, fs.readFileSync(attachment.file))
      })
      const buffer = await zip.generateAsync({ type: 'nodebuffer' })
      return buffer
    } else {
      return credential
    }
  }

  /**
   * Single step for a verified Matrix, logged-in Matrix user to verify credentials:
   * Create a room, invite the bot, send the credential, and set a callback function
   * to handle messages in the room from the bot.
   *
   * @param {string} userIdToInvite - Matrix user ID to invite to the new room
   * @param {string} credential - credential in JSON format
   * @param {Function} callback - callback function
   * @param {*} data - data to pass to the callback (usually this Client object)
   * @returns {*} roomId and eventId
   */
  async submitCredentialForVerification (userIdToInvite, credential, callback, data = undefined) {
    const roomId = await this.createRoomAndInvite(userIdToInvite)
    this.createRoomCallback(roomId, callback, data)
    const eventId = await this.sendCredentialToRoom(roomId, credential)
    return { roomId, eventId }
  }

  /**
   * Single step for a guest Matrix user to verify credentials:
   * Creates a guest, initializes the client, joins the lobby, sends the magic
   * word, leaves the lobby, accepts an invite to a private room, posts the
   * credential there, and sets a callback for the result.
   *
   * @param {string} baseUrl - Matrix server URL
   * @param {string} lobbyRoomIdOrAlias - a public room where bots are waiting to service requests
   * @param {string} lobbyPhrase - the magic word which causes a bot to invite you to a private room
   * @param {JSON} credential - the credential which you wish to verify
   * @param {Function} responseRoomCallback - a callback function triggered when messages arrive in the private room
   * @returns {string} event ID
   */
  async submitCredentialForVerificationAsGuest (baseUrl, lobbyRoomIdOrAlias, lobbyPhrase, credential, responseRoomCallback) {
    // create a guest
    const guest = await helpers.createMatrixGuest(baseUrl)
    // initialize the Matrix client with the standard callback for being invited to a room
    await this.init(baseUrl, guest.access_token, guest.user_id, invitedRoomCallback)
    // Join the lobby
    const result = await helpers.joinMatrixRoomAsGuest(baseUrl, lobbyRoomIdOrAlias, guest.access_token)
    const lobbyRoomId = result.room_id
    // Set the things we're going to need in the standard callback
    this._caelumCredentialToSend = credential
    this._caelumResponseRoomCallback = responseRoomCallback
    // Send the magic phrase which causes the daemon to issue an invite to the private room
    return this.sendTextMessage(lobbyRoomId, lobbyPhrase)
    // caller will receive messages in the responseRoomCallback
  }

  /**
   * Single step for a guest Matrix to ask for credentials.
   * Creates a guest, initializes the client to join private rooms
   * as invited, and sets a callback for the result.
   *
   * @param {string} baseUrl - Matrix server URL
   * @param {Function} responseRoomCallback - a callback function triggered when messages arrive in the private room
   * @returns {*} guest account info, or undefined if the guest account is not created
   */
  async registerForAuthenticationAsGuest (baseUrl, responseRoomCallback) {
    // create a guest
    const guest = await helpers.createMatrixGuest(baseUrl)
    if (!guest.access_token) {
      return undefined
    }

    // Consent to the TOS, which would otherwise cause failure when this guest is invited to a room.
    await helpers.consentToMatrixTOS(baseUrl, guest.access_token)
    // initialize the Matrix client with the standard callback for being invited to a room
    await this.init(baseUrl, guest.access_token, guest.user_id, invitedRoomCallback)
    // Mark this user as guest
    this.client.setGuest(true)
    // Set the things we're going to need in the standard callback
    // caller will receive messages in the responseRoomCallback
    this._caelumResponseRoomCallback = responseRoomCallback
    // But we're not sending anything, so don't set the credential
    return guest
  }

  /**
   * Creates the credential request body under the
   * Verifiable Credential Request (VCR) Standard
   * https://gitlab.com/caelum-tech/lorena/lorena-sdk/blob/master/src/verifiable-credential-request-standard.md
   *
   * @param {string} uri - URI or URL of type(s) being requested
   * @param {string} identity - DID of the entity requesting the VC
   * @returns {object} credential request body
   */
  getCredentialRequest (uri, identity) {
    return vcr.vcrBody(this.client.isGuest() ? 'matrix-guest' : 'matrix-user', this.client.getUserId(), uri, identity)
  }

  /**
   * Creates the credential request URL under the
   * Verifiable Credential Request (VCR) Standard
   * https://gitlab.com/caelum-tech/lorena/lorena-sdk/blob/master/src/verifiable-credential-request-standard.md
   *
   * @param {string} uri - URI or URL of type(s) being requested
   * @param {string} identity - DID of the entity requesting the VC
   * @param {string} baseUrl - base URL of the wallet / PDS / identity container
   * @param {string} encodeType - type of encoding to use for the body
   * @returns {string} VCR URL
   */
  getUrlCodifiedCredentialRequest (uri, identity, baseUrl, encodeType = 'json') {
    return vcr.encodeVCR(baseUrl, this.getCredentialRequest(uri, identity), encodeType)
  }

  /**
   * Authenticate by providing credentials to a third party
   * Creates a guest, initializes the client to join private rooms
   * as invited, and sets a callback for the result.
   *
   * @param {string} baseUrl - Matrix server URL
   * @param {string} lobbyRoomIdOrAlias - #lobby or !random:matrix.server
   * @param {string} lobbyPhrase - the command which causes the bot to set up a room
   * @param {string} credentialRequest - the name of the desired credential
   * @param {JSON} credential - the credential with which to authenticate
   * @returns {*} guest account info
   */
  async authenticateAsGuest (baseUrl, lobbyRoomIdOrAlias, lobbyPhrase, credentialRequest, credential) {
    // create a guest
    const guest = await helpers.createMatrixGuest(baseUrl)
    // initialize the Matrix client with the standard callback for being invited to a room
    await this.init(baseUrl, guest.access_token, guest.user_id, invitedRoomCallback)
    // Mark this user as guest
    this.client.setGuest(true)

    // Set the things we're going to need in the standard callback
    // we want to send a credential when we're invited to a room
    this._caelumCredentialToSend = credential
    // But we're not looking for responses from the third party, so we don't set a callback.

    const result = await helpers.joinMatrixRoomAsGuest(baseUrl, lobbyRoomIdOrAlias, guest.access_token)
    const lobbyRoomId = result.room_id

    // post the message in the specified lobby. If the third party (who requested the authentication)
    // is also a guest, then its user name must be mentioned as well so it is invited to the room.
    // Send the magic phrase which causes the daemon to issue an invite to the private room
    const text = credentialRequest.transport.type === 'matrix-guest'
      ? `${lobbyPhrase} ${credentialRequest.transport.address}` : lobbyPhrase
    const message = await this.sendTextMessage(lobbyRoomId, text)
    console.log(message)

    // Append other interesting data to the guest as a return value
    guest.messageId = message
    guest.roomId = result.room_id
    return guest
  }

  /**
   * Leave the room, de-registering room callback
   *
   * @param {string} roomId - Matrix room to leave
   * @returns {boolean} success
   */
  async leave (roomId) {
    // deregister callback
    roomCallbacks[roomId] = undefined
    // leave room
    await this.client.leave(roomId)
  }

  /**
   * Close session. Warning: will make the access token subsequently invalid.
   */
  async close () {
    // Stop matrix client
    this.client.stopClient()

    // Guests are not allowed to log out ¯\_(ツ)_/¯
    if (!this.client.isGuest()) {
      await this.client.logout()
    }
    this.client = undefined
  }
}

module.exports = { Client }
