const helpers = require('@caelum-tech/lorena-matrix-helpers')
const { Client } = require('../src/client.js')
const config = require('../src/config.js')
const chai = require('chai')

// Configure chai
chai.should()
chai.use(require('chai-string'))

/**
 * construct a Verifiable Credential object for testing
 *
 * @param {string} credentialType the type of credential being created
 * @param {*} credentialSubject0 the first (and only) credentialSubject
 * @returns {*} credential
 */
function constructTestCredential (credentialType, credentialSubject0) {
  return {
    '@context': [
      'https://www.w3.org/2018/credentials/v1',
      'https://www.w3.org/2018/credentials/examples/v1'
    ],
    type: [
      'VerifiableCredential',
      credentialType
    ],
    claims: [
      {
        credentialStatus: {
          id: 'https://lorena.caelumlabs.com/credentialTypes',
          type: credentialType
        },
        type: [
          'VerifiableCredential',
          credentialType
        ],
        credentialSubject: [
          credentialSubject0
        ],
        id: 'did:lor:2828283828327',
        issuanceDate: '1570612530036',
        issuer: 'did:lrn:123124212#caelum-identity'
      }
    ],
    id: 'did:lor:3ff9f09422d783244a35f8efe5e122e3bd050174aacd386663dd0f4b91211e9b',
    issuer: 'did:lor:3ff9f09422d783244a35f8efe5e122e3bd050174aacd386663dd0f4b91211e9b',
    proof: [],
    nonRevocationProof: [
      {
        type: credentialType,
        created: '2019-12-03T16:01:21.751Z',
        verificationMethod: 'u64:BAee_Hj-P0mhMtvU3-s386HCw_tFSxi3ebAxqs9NcMaNoHIFYOZzNrzPnMfjhhy3_B4sQZYH9-abjOO1DdcEGLyunvg46iTyUkWTEx-tcA85PVAVRdD95yDrHfmiBIMSVW7ImjNMqhQVSyi-r0TyK3g',
        signatureValue: 'u64:BAee_Hj-P0mhMtvU3-s386HCw_tFSxi3ebAxqs9NcMaNoHIFYOZzNrzPnMfjhhy3_B4sQZYH9-abjOO1DdcEGLyunvg46iTyUkWTEx-tcA85PVAVRdD95yDrHfmiBIMSVW7ImjNMqhQVSyi-r0TyK3g',
        zenroom: {
          draft: 'u64:eyJjcmVkZW50aWFsU3RhdHVzIjp7ImlkIjoiaHR0cHM6Ly9sb3JlbmEuY2FlbHVtbGFicy5jb20vY3JlZGVudGlhbHMiLCJ0eXBlIjoiQ2FlbHVtRW1haWxDcmVkZW50aWFsIiwiY3VycmVudFN0YXR1cyI6InBlbmRpbmciLCJzdGF0dXNSZWFzb24iOiJzZWxmLWlzc3VlZCJ9LCJ0eXBlIjpbIlZlcmlmaWFibGVDcmVkZW50aWFsIiwiQ2FlbHVtRW1haWxDcmVkZW50aWFsIl0sImNyZWRlbnRpYWxTdWJqZWN0IjpbeyJ0eXBlIjoiQ2FlbHVtRW1haWxDcmVkZW50aWFsIiwiZW1haWwiOiJjaHVjaytjb3dAY2FlbHVtbGFicy5jb20iLCJmaXJzdCI6IkNodWNrIiwibGFzdCI6IkNvdyJ9XSwiaWQiOiJkaWQ6bG9yOjNmZjlmMDk0MjJkNzgzMjQ0YTM1ZjhlZmU1ZTEyMmUzYmQwNTAxNzRhYWNkMzg2NjYzZGQwZjRiOTEyMTFlOWIiLCJpc3N1YW5jZURhdGUiOiIyMDE5LTEyLTAzVDE2OjAxOjIxLjc1MVoiLCJpc3N1ZXIiOiJkaWQ6bG9yOjNmZjlmMDk0MjJkNzgzMjQ0YTM1ZjhlZmU1ZTEyMmUzYmQwNTAxNzRhYWNkMzg2NjYzZGQwZjRiOTEyMTFlOWIifQ',
          signature: {
            r: 'u64:BhUmzxK3s3I3s2uPkpEgJ9HWCNRMYnkFeqltjf0IkSpnvW1dZOp-7O5j7hI14ZdlE6JHyL0NpYc',
            s: 'u64:HbrKkhjfYIYqPPTLNM_mDfHYMhcyCaPQs-yPt_ocw12IXgLQ4o74TnFMpXCpdgYFYWlTCbMaNOg'
          },
          public_key: 'u64:BAee_Hj-P0mhMtvU3-s386HCw_tFSxi3ebAxqs9NcMaNoHIFYOZzNrzPnMfjhhy3_B4sQZYH9-abjOO1DdcEGLyunvg46iTyUkWTEx-tcA85PVAVRdD95yDrHfmiBIMSVW7ImjNMqhQVSyi-r0TyK3g'
        }
      }
    ]
  }
}

/**
 * Construct a verifiable credential of type CaelumEmailCredential for testing purposes
 *
 * @returns {*} credential
 */
function constructTestCaelumEmailCredential () {
  const credentialType = 'CaelumEmailCredential'
  const credential = constructTestCredential(credentialType, {
    type: credentialType,
    email: 'verification+lorena-matrix-client@caelumlabs.com',
    last: 'Caelum Labs',
    first: 'Verification'
  })
  return credential
}

describe('Known User Flow', function () {
  let userId, accessToken, client

  before(async () => {
    // If we're supplied with an access token, just use it
    if (config.matrixClientAccessToken) {
      accessToken = config.matrixClientAccessToken
      userId = config.matrixClientUserId
    } else {
      let password

      // if we have a username and password, use those and log in
      if (config.matrixClientUserId && config.matrixClientPassword) {
        userId = config.matrixClientUserId
        password = config.matrixClientPassword
      } else {
        // otherwise, create a test account
        const clientMatrixUser = await helpers.createMatrixTestUser(config.matrixServerURL)
        userId = helpers.buildMatrixUserId(helpers.matrixServerNameFromUrl(config.matrixServerURL), clientMatrixUser.username)
        password = clientMatrixUser.password
      }

      // log in with the username and password to get a new access token
      const session = await helpers.loginMatrixUser(config.matrixServerURL, userId, password)
      accessToken = session.access_token
    }
  })

  it('should connect to the matrix server using the access token', async () => {
    client = new Client()
    await client.init(config.matrixServerURL, accessToken, userId)
    client.client.should.not.be.undefined
  })

  it('should send a credential to the room', async () => {
    // should send a credential to the room, with the callback handler for responses
    const { roomId, eventId } = await client.submitCredentialForVerification(config.matrixDaemonUserId, constructTestCaelumEmailCredential(), responseRoomCallback, client)
    console.log(`${roomId} ${eventId}`)
  })

  xit('should close the client', async () => {
    // If we're conserving the access token, don't close the connection or it'll kill the access token
    if (config.matrixClientAccessToken) {
      client.client = undefined
      client = undefined
    } else {
      await client.close()
    }
  })
})

/**
 * Called when a message is posted to a specific room
 *
 * @param {*} event event to process
 * @param {*} data user data (client object)
 * @returns {boolean} success
 */
function responseRoomCallback (event, data) {
  // should receive the data along with the callback
  data.should.not.be.undefined
  const client = data

  const body = event.content.body
  let newCredential

  try {
    newCredential = JSON.parse(body)
  } catch (e) {}

  if (newCredential) {
    // the callback function sees that the signed VC is available :+2: and downloads it
    newCredential.nonRevocationProof[1].created.should.not.be.undefined
    // the client leaves the room (and quits getting callbacks)
    return client.leave(event.room_id)
  } else {
    // the callback function sees that the request was received :+1:
    body.should.contain('Progress: ')
  }
}

/**
 * Called when the user is invited to a new room. (Matrix guest user flow)
 *
 * @param {*} roomId Matrix room ID
 * @param {*} data the client
 */
function invitedRoomCallback (roomId, data) {
  // should receive the data along with the callback
  data.should.not.be.undefined
  const client = data

  // set up a callback specific to this room
  client.createRoomCallback(roomId, responseRoomCallback, data)
  // Send the credential
  client.sendCredentialToRoom(roomId, constructTestCaelumEmailCredential())
}

describe('Guest User Flow', function () {
  let guest, client, lobbyRoomId

  before(async () => {
    guest = await helpers.createMatrixGuest(config.matrixServerURL)
  })

  it('should initialize the client', async () => {
    client = new Client()
    await client.init(config.matrixServerURL, guest.access_token, guest.user_id, invitedRoomCallback)
  })

  it('should join the lobby', async () => {
    const result = await helpers.joinMatrixRoomAsGuest(config.matrixServerURL, config.matrixLobby, guest.access_token)
    console.log(result)
    lobbyRoomId = result.room_id
  })

  it('should post the magic word to the lobby', async () => {
    // the bot will be stalking the lobby waiting for this word to be said
    await client.sendTextMessage(lobbyRoomId, 'verifyEmail!')
  })

  it('should accept an invitation to a new room with the bot', async () => {
    // happens in client.init() which calls invitedRoomCallback()
  })

  xit('should establish a secure channel by sending shared secret signed with private key', async () => {
  })

  xit('should establish a secure channel by verifying signature of shared secret signed with private key', async () => {
  })

  it('should respond in the new room with the credential', async () => {
    // happens in invitedRoomCallback()
  })

  it('should see a response from the bot to check the email', async () => {
    // happens in responseRoomCallback()
  })

  it('should receive the proof from the bot', async () => {
    // happens in responseRoomCallback()
  })

  xit('should leave the lobby', async () => {
    // leave the lobby
    await client.leave(lobbyRoomId)
  })

  xit('should leave the private room', async () => {
    // leave the private room
  })

  // Disabled because otherwise the callbacks never happen
  xit('should close the client', async () => {
    await client.close()
  })
})

describe('One-Shot Guest User Flow', function () {
  // creates a client, joins the lobby, sends the magic word, leaves the lobby,
  // accepts an invite to a private room, posts the credential there, and sets
  // a callback for the result.
  it('should submit a credential for verification and receive result via callback', async () => {
    const client = new Client()
    await client.submitCredentialForVerificationAsGuest(config.matrixServerURL, config.matrixLobby, 'verifyEmail!', constructTestCaelumEmailCredential(), responseRoomCallback)
  })

  it('should see a response from the bot to check the email', async () => {
    // happens in responseRoomCallback()
  })

  it('should receive the proof from the bot', async () => {
    // happens in responseRoomCallback()
  })

  it('should leave the private room', async () => {
    // happens in responseRoomCallback()
  })
})

describe('One-Shot User Flow with Attachments', function () {
  // creates a user, uploads credential file, creates a private room, invites daemon,
  // posts the credential there, and sets a callback for the result.
  it('should submit a credential for verification and receive result via callback', async () => {
    const client = new Client()
    const testUser = await helpers.createMatrixTestUser(config.matrixServerURL)
    await client.init(config.matrixServerURL, testUser.access_token, testUser.user_id)
    const credentialType = 'CaelumResidencyCredential'
    // reverso_3 and anverso_3 are too big, which returns a 413 error because of Matrix config.
    const back = 'reverso_1.png'
    const front = 'anverso_1.png'
    const credential = constructTestCredential(credentialType, {
      type: credentialType,
      idBack: back,
      idFront: front,
      postalCode: '08003'
    })
    const payload = await client.makeSingleCredentialFile(credential, [{ name: front, file: `./test/artifacts/${front}` }, { name: back, file: `./test/artifacts/${back}` }])
    await client.submitCredentialForVerification(config.matrixDaemonUserId, payload, responseRoomCallback, client)
  })
})

/**
 * Called when the website receives a credential
 *
 * @param {*} event event to handle
 * @param {*} data user data (client object)
 */
function authenticationCallback (event, data) {
  // should receive the data along with the callback
  data.should.not.be.undefined

  const body = event.content.body
  try {
    const newCredential = JSON.parse(body)
    console.log(newCredential)
    // test here
  } catch (e) {}
}

describe('Third party authentication', function () {
  let userClient, thirdClient, thirdGuest, credentialRequest, event
  const baseUrl = 'https://playground.did'
  const vcType = 'lor:CaelumEmailCredential'
  const requestorDID = 'did:lor:third-party'

  it('should allow the third party to connect as a guest and wait for invitations', async () => {
    thirdClient = new Client()
    thirdGuest = await thirdClient.registerForAuthenticationAsGuest(config.matrixServerURL, authenticationCallback)
    thirdGuest.user_id.should.not.be.undefined
  })

  it('should create credential request body', async () => {
    credentialRequest = thirdClient.getCredentialRequest(vcType, requestorDID)
    credentialRequest.type.should.eq(vcType)
    credentialRequest.identity.should.eq(requestorDID)
  })

  it('should create credential request with CBOR format', async () => {
    const credentialRequestCbor = thirdClient.getUrlCodifiedCredentialRequest(vcType, requestorDID, baseUrl, 'cbor')
    credentialRequestCbor.should.not.be.empty
    credentialRequestCbor.should.startWith(baseUrl)
  })

  it('should create credential request with JSON format', async () => {
    const credentialRequestJson = thirdClient.getUrlCodifiedCredentialRequest(vcType, requestorDID, baseUrl, 'json')
    credentialRequestJson.should.not.be.empty
    credentialRequestJson.should.startWith(baseUrl)
  })

  it('should not create credential request with invalid format', async () => {
    const credentialRequestInvalid = thirdClient.getUrlCodifiedCredentialRequest(vcType, requestorDID, baseUrl, 'invalid')
    credentialRequestInvalid.should.be.empty
  })

  xit('should send the required credentials via Matrix', async () => {
    // Doesn't work because matrix-js-sdk does not support multiple simultaneous clients
    userClient = new Client()
    // we know the QR code with the credentialRequest because we scanned it off the third-party website
    event = await userClient.authenticateAsGuest(config.matrixServerURL, '#lorena-client-test:matrix.org', 'authenticate!', credentialRequest, constructTestCaelumEmailCredential)
    console.log(event)
    // event.room_id.should.not.be.undefined
    // event.message.should.eq(credential.something)
  })

  xit('third party should receive the required credentials via Matrix', async () => {
    // should happen in authenticationCallback()

    // unfortunately, it appears that we can't have two Matrix JS SDK clients
    // running in the same process, so this test never passes.
  })

  xit('should have nobody left in the room at the end', async () => {

  })

  it('should close the client', async () => {
    thirdClient.close()
  })
})
