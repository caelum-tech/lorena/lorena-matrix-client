# Lorena Matrix Client
Javascript client for interfacing with the Lorena system via Matrix

|Branch|Pipeline|Coverage|
|:-:|:-:|:-:|
|[`master`](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client/tree/master)|[![pipeline status](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client/badges/master/pipeline.svg)](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client/commits/master)|[![coverage report](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client/badges/master/coverage.svg)](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client/commits/master)|

## Run tests

Install dependencies
```bash
npm i
```

Run the tests
```bash
npm test
```

## Usage
```bash
npm install -s @caelum-tech/lorena-matrix-client

```

```JavaScript
const Client = require('@caelum-tech/lorena-matrix-client')

// implement callback function to receive responses from the verification bot
// event: the Matrix event containing the message
// data: whatever data you need to pass along
function callback (event, data) => {
  const body = event.content.body
  let credential

  try {
    credential = JSON.parse(body)
  } catch (e) {}

  if (credential) {
    // The proof will be included in the credential
    console.log(credential.proof[1])
  } else {
    // Other messages sent by the daemon (email sent, errors, etc)
    console.log(body)
  }
}

// your credential to verify
const credential = {
  '@context': [
    'https://www.w3.org/2018/credentials/v1',
    'https://www.w3.org/2018/credentials/examples/v1'
  ],
  type: [
    'VerifiableCredential',
    'EmailCredential'
  ]
  // etc
}

// Instantiate the client object
client = new Client()
```

### For validated Matrix users
```JavaScript
// Use an existing Matrix session token for a verified Matrix account
await client.init(matrixServerURL, accessToken, userId)

// Creates a new room, invites the verifier bot, sends credential, specifies callback for results, pass along my own data needed
const { roomId, eventId } = await client.submitCredentialForVerification(botMatrixId, credential, callback, 'myData')
```

### For Guest users (who have no existing Matrix account)
```JavaScript
// Create a guest Matrix user, join lobby, post magic word,
// wait for invite to room, post credential, and receive callback
// with responses
await client.submitCredentialForVerificationAsGuest(config.matrixServerURL, '#lorena-lobby-test:matrix.org', 'verifyEmail!', credential, responseRoomCallback)
```

### Finally...
```javascript
// the client leaves the room (and quits getting callbacks)
// This can be called from within the responseRoomCallback
client.leave(roomId)

// Shut down the client connection to Matrix
await client.close()
```

## See also
[Lorena Matrix Daemon](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon)
